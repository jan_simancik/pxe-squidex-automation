import sys
import time
import requests

# Global variables
client_id = 'pxe-parc4u:python'
client_secret = 'odawvvcufpcl7erbc3cqapwxqqnzpkwy9pthfuo1doox'
admin_client = 'admin-restore'
admin_secret = 'NikomuToNepoviem123@'
check_client = '5f550dfa3019a80001a75205'
check_secret = 'dypoc2dqldu5pxool0xsdtqqfeyztnniccopon5nt3mx'
squidex_url = ("https://squidex.lnd.bz/identity-server/connect/token",
               "https://squidex-prod.lnd.bz/identity-server/connect/token")
backup_url = "https://squidex-preview.lnd.bz/api/apps/pxe-parc4u/backups"
archive_url = "https://squidex-prod.lnd.bz/api/apps/pxe-parc4u/"
restore_url = "https://squidex-prod.lnd.bz/api/apps/restore"


def payload_data(client, secret):
    # Data for the squidex_token function
    payload = 'grant_type=client_credentials&' \
              f'client_id={client}&' \
              f'client_secret={secret}&' \
              'scope=squidex-api'
    return payload


def squidex_token(url, payload):
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}

    response = requests.request("POST", url, headers=headers, data=payload)
    # Extract only token from the response
    return response.json()['access_token']


def check_num_of_backups(url, token):
    # Count of the backups
    headers = {'Authorization': f'Bearer {token}'}

    response = requests.request("GET", url, headers=headers)

    return len(response.json()['items'])


def create_backup(url, token):
    headers = {'Authorization': f'Bearer {token}'}

    response = requests.request("POST", url, headers=headers)

    if response.status_code in (200, 201, 202, 203, 204):
        print(f"Backup created.")


def delete_backup(url, token):
    headers = {'Authorization': f'Bearer {token}'}

    response = requests.request("GET", url, headers=headers)
    # The last backup ID and date
    to_delete = response.json()['items'][-1]['id']
    date_of_creation = response.json()['items'][-1]['stopped']

    response = requests.request("DELETE", url + '/' + to_delete, headers=headers)

    if response.status_code in (200, 201, 202, 203, 204):
        print(f'Last backup deleted, because exceeded max number of a backups!\n'
              f'ID: {to_delete}\nDate Of Creation: {date_of_creation}')
    else:
        print('Delete of the last backup FAILED!')
        sys.exit(255)


def app_exist(url):
    token = squidex_token("https://squidex-prod.lnd.bz/identity-server/connect/token",
                          payload_data(check_client,
                                       check_secret
                                       )
                          )
    headers = {
        'Authorization': f'Bearer {token}'
    }
    app_list = []
    response = requests.request("GET", url, headers=headers, data={})
    # Gather an active apps list
    for app in response.json():
        app_list.append(app['name'])

    return app_list


def backup_download_url(url, token):
    # Extract the newest backup URL
    headers = {'Authorization': f'Bearer {token}'}

    response = requests.request("GET", url, headers=headers)

    return response.json()['items'][0]['_links']['download']['href']


def archive_app(url, token):
    # Archive an old app
    headers_delete = {
        'Authorization': f'Bearer {token}'
    }

    payload_delete = {}

    response = requests.request("DELETE", url, headers=headers_delete, data=payload_delete)

    if response.text == '':
        print('Old app deleted.')


def upload_backup(url, token, download_url):
    headers_restore = {
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {token}'
    }
    # Payload have to be in format of the RAW JSON
    payload_restore = "{\n\"name\": \"pxe-parc4u\",\n\"url\": \"%s\"\n}" % download_url

    # Restore app
    response = requests.request("POST", url, headers=headers_restore, data=payload_restore)

    if response.text == '':
        print('Restore DONE.')


if __name__ == '__main__':
    # Token for the pxe-parc4u:python client
    auth_token = squidex_token("https://squidex-preview.lnd.bz/identity-server/connect/token",
                               payload_data(client_id,
                                            client_secret)
                               )
    # Token for the admin client (define in the appsettings.json)
    admin_auth_token = squidex_token("https://squidex-prod.lnd.bz/identity-server/connect/token",
                                     payload_data(admin_client,
                                                  admin_secret)
                                     )
    # First it has to check if number of the backups not exceeded 9 ( 10 is maximum which Squidex allow)
    if check_num_of_backups("https://squidex-preview.lnd.bz/api/apps/pxe-parc4u/backups", auth_token) >= 9:
        delete_backup("https://squidex-preview.lnd.bz/api/apps/pxe-parc4u/backups", auth_token)
    # Create backup on the dev.Squidex
    create_backup("https://squidex-preview.lnd.bz/api/apps/pxe-parc4u/backups", auth_token)
    time.sleep(5)
    # Loop to delete app (it has to be loop because there is some delay)
    while 'pxe-parc4u' in app_exist('https://squidex-prod.lnd.bz/api/apps'):
        archive_app(archive_url, admin_auth_token)
        time.sleep(1)
    # Loop to create an app, another delay workaround
    while 'pxe-parc4u' not in app_exist('https://squidex-prod.lnd.bz/api/apps'):
        upload_backup(restore_url,
                      admin_auth_token,
                      f"https://squidex-preview.lnd.bz{backup_download_url(backup_url, auth_token)}"
                      )
        time.sleep(2)
